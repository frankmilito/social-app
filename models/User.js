import mongoose from "mongoose";
import bcrypt from "bcrypt";

const userSchema = new mongoose.Schema(
  {
    // firstName: {
    //   type: String,
    //   required: [true, "First Name is required"],
    //   min: 2,
    //   max: 50,
    // },
    // lastName: {
    //   type: String,
    //   required: [true, "Last Name is required"],
    //   min: 2,
    //   max: 50,
    // },
    // email: {
    //   type: String,
    //   required: [true, "Email is required"],
    //   max: 50,
    //   unique: true,
    // },
    // password: {
    //   type: String,
    //   required: [true, "Password is required"],
    //   max: 5,
    // },
    // picturePath: {
    //   type: String,
    //   default: "",
    // },
    // friends: {
    //   type: Array,
    //   default: [],
    // },
    // location: String,
    // occupation: String,
    // viewedProfile: Number,
    // impressions: Number,
    username: {
      type: String,
      required: [true, "Username is required"],
      min: 2,
      max: 50,
    },
    email: {
      type: String,
      required: [true, "Email is required"],
      max: 50,
      unique: true,
    },
    password: {
      type: String,
      required: [true, "Password is required"],
      max: 5,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.methods.correctPassword = async function (password, dbPassword) {
  return await bcrypt.compare(password, dbPassword);
};

export default mongoose.model("User", userSchema);
