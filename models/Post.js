import mongoose from "mongoose";

const postSchema = new mongoose.Schema(
  {
    //Getting user by reference
    userId: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },

    description: String,

    picturePath: String,

    userPicturePath: String,

    likes: {
      type: Map,
      of: Boolean,
    },
    comments: {
      type: Array,
      default: [],
    },
  },
  {
    timestamps: true,
  }
);

// postSchema.pre("aggregate", function (next) {
//   this.pipeline().sort
// });

export default mongoose.model("Post", postSchema);
