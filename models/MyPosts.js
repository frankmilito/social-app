import mongoose from "mongoose";

const myPostSchema = new mongoose.Schema(
  {
    //Getting user by reference
    userId: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
    },
    updatedAt: {
      type: Date,
    },
  }
  // {
  //   timestamps: true,
  // }
);

// postSchema.pre("aggregate", function (next) {
//   this.pipeline().sort
// });

export default mongoose.model("MyPosts", myPostSchema);
