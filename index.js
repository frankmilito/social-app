import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import dotenv from "dotenv";
import multer from "multer";
import helmet from "helmet";
import morgan from "morgan";
import path from "path";
import { fileURLToPath } from "url";
import * as auth from "./controllers/auth.js";
import * as postsController from "./controllers/posts.js";
import authRoutes from "./routes/auth.js";
import userRoutes from "./routes/user.js";
import postRoutes from "./routes/posts.js";
import myPostRoutes from "./routes/myPosts.js";
import { verifyToken } from "./middlware/auth.js";
import User from "./models/User.js";
import Post from "./models/Post.js";
import globalErrorHandler from "./controllers/error.js";
import { users, posts } from "./data/index.js";
// configurations
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
dotenv.config();
const app = express();

app.use(express.json());
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));
// app.use(morgan("common"));
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());
app.use("/assets", express.static(path.join(__dirname, "public/assets")));

// storage setup
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/assets");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

/* Routes with files */
app.post("/auth/register", upload.single("picture"), auth.register);
app.post(
  "/posts",
  verifyToken,
  upload.single("picture"),
  postsController.createPost
);
app.use("/auth", authRoutes);
app.use("/users", userRoutes);
app.use("/posts", postRoutes);
app.use("/myposts", myPostRoutes);
//mongoose setup

// const deleteAll = async () => {
//   await User.deleteMany();
//   await Post.deleteMany();
// };
// deleteAll();
const PORT = process.env.port || 3005;
mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(PORT, () =>
      console.log("Connected to DB, istening on port " + PORT)
    );
    /* ADD DATA ONE TIME */

    // User.insertMany(users);
    // Post.insertMany(posts);
  })
  .catch((err) => console.log(err));

app.use(globalErrorHandler);
