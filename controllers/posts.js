import Post from "../models/Post.js";
import User from "../models/User.js";
import AppError from "../utils/appError.js";
import catchAsync from "../utils/catchAsync.js";

export const createPost = catchAsync(async (req, res) => {
  const { userId, description, picturePath } = req.body;
  const user = await User.findById(userId);

  await Post.create({
    userId,
    firstName: user.firstName,
    lastName: user.lastName,
    description,
    picturePath,
    userPicturePath: user.picturePath,
    location: user.location,
    likes: {},
    comments: [],
  });

  const posts = await Post.aggregate([{ $sort: { createdAt: -1 } }]);

  res.status(201).json({
    status: "success",
    data: {
      posts,
    },
  });
});

export const getFeedPosts = catchAsync(async (req, res) => {
  const posts = await Post.aggregate([
    { $sort: { createdAt: -1 } }, // Sort by descending created date
  ]);
  res.status(200).json({
    status: "success",
    data: {
      posts,
    },
  });
});

export const getUserPosts = catchAsync(async (req, res) => {
  const { userId } = req.params;
  const posts = await Post.find({ userId });
  res.status(200).json({
    status: "success",
    data: {
      posts,
    },
  });
});

export const likePost = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { userId } = req.body;
  const post = await Post.findById(id);
  const isLiked = post.likes.get(userId);
  if (isLiked) {
    post.likes.delete(userId);
  } else {
    post.likes.set(userId, true);
  }

  await Post.findByIdAndUpdate(
    id,
    {
      likes: post.likes,
    },
    {
      new: true,
    }
  );

  const posts = await Post.aggregate([{ $sort: { createdAt: -1 } }]);
  res.status(200).json({
    status: "success",
    data: {
      posts,
    },
  });
});
