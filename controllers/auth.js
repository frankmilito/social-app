import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import User from "../models/User.js";
import AppError from "../utils/appError.js";
import catchAsync from "../utils/catchAsync.js";

export const register = catchAsync(async (req, res, next) => {
  const {
    // firstName,
    // lastName,
    // email,
    // password,
    // picturePath,
    // friends,
    // location,
    // occupation,
    username,
    password,
    email,
  } = req.body;

  const salt = await bcrypt.genSalt();
  const passwordHash = await bcrypt.hash(password, salt);
  let user;
  user = await User.findOne({ email });
  if (user) {
    return next(new AppError("User already registered, please login", 400));
  }
  user = await User.create({
    // firstName,
    // lastName,
    // email,
    // password: passwordHash,
    // picturePath,
    // friends,
    // location,
    // occupation,
    // viewedProfile: Math.floor(Math.random() * 10000),
    // impressions: Math.floor(Math.random() * 10000),
    email,
    username,
    password: passwordHash,
  });
  // return next(new AppError("Somehting happend", 400));
  const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);
  res.status(200).json({
    status: "success",
    data: {
      user,
      token,
    },
  });
});

export const login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) return next(new AppError("User not found", 404));

  if (!(await user.correctPassword(password, user.password)))
    return next(new AppError("Invalid credentials", 400));
  const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);

  user.password = undefined;
  res.status(200).json({
    status: "success",
    data: {
      user,
      token,
    },
  });
});
