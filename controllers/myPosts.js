import User from "../models/User.js";
import MyPosts from "../models/MyPosts.js";
import AppError from "../utils/appError.js";
import catchAsync from "../utils/catchAsync.js";

export const createPost = catchAsync(async (req, res) => {
  const { userId, title, content, username } = req.body;
  const user = await User.findById(userId);
  await MyPosts.create({
    userId,
    title,
    content,
    username,
    updatedAt: null,
    createdAt: Date.now() + 10 * 60 * 1000,
  });

  const posts = await MyPosts.aggregate([{ $sort: { createdAt: -1 } }]);

  res.status(201).json({
    status: "success",
    data: {
      posts,
    },
  });
});

export const getMyPosts = catchAsync(async (req, res) => {
  const posts = await MyPosts.aggregate([
    { $sort: { createdAt: -1 } }, // Sort by descending created date
  ]);
  res.status(200).json({
    status: "success",
    data: {
      posts,
    },
  });
});
export const editPost = catchAsync(async (req, res) => {
  const params = {
    ...req.body,
    updatedAt: Date.now() + 10 * 60 * 1000,
  };
  const doc = await MyPosts.findByIdAndUpdate(req.params.id, params, {
    new: true,
    runValidators: true,
  });
  res.status(200).json({
    status: "success",
    data: {
      data: doc,
    },
  });
});
export const deletePost = catchAsync(async (req, res) => {
  console.log(req.body);
  const doc = await MyPosts.findByIdAndDelete(req.params.id, {
    new: true,
    runValidators: true,
  });
  res.status(204).json({
    status: "success",
    data: null,
  });
});
