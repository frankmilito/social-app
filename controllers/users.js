import User from "../models/User.js";
import AppError from "../utils/appError.js";
import catchAsync from "../utils/catchAsync.js";

export const getUser = catchAsync(async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id);
  res.status(200).json({
    status: "success",
    data: user,
  });
});

export const getUserFriends = catchAsync(async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id);
  const friends = await Promise.all(
    user.friends.map((id) => User.findById(id))
  );
  const formattedFriends = friends.map(
    ({ _id, location, firstName, lastName, occupation, picturePath }) => {
      return { _id, location, firstName, lastName, occupation, picturePath };
    }
  );
  res.status(200).json({
    status: "success",
    formattedFriends,
  });
});

export const addRemoveFriend = catchAsync(async (req, res) => {
  const { id, friendId } = req.params;
  console.log(id, friendId);
  const user = await User.findById(id);
  const friend = await User.findById(friendId);

  if (user.friends.includes(friendId)) {
    user.friends = user.friends.filter((id) => id !== friendId);
    friend.friends = friend.friends.filter((id) => id !== id);
  } else {
    user.friends.push(friendId);
    friend.friends.push(id);
  }

  await friend.save();
  await user.save();

  const friends = await Promise.all(
    user.friends.map((id) => User.findById(id))
  );

  const formattedFriends = friends.map(
    ({ _id, location, firstName, lastName, occupation, picturePath }) => {
      return { _id, location, firstName, lastName, occupation, picturePath };
    }
  );

  res.status(200).json({ status: "success", formattedFriends });
});
