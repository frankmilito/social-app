import express from "express";
import * as userController from "../controllers/users.js";
import { verifyToken } from "../middlware/auth.js";

const router = express.Router();

router.get("/:id", verifyToken, userController.getUser);
router.get("/:id/friends", userController.getUserFriends);
router.patch("/:id/:friendId", verifyToken, userController.addRemoveFriend);

export default router;
