import express from "express";
import * as myPostsController from "../controllers/myPosts.js";
import { verifyToken } from "../middlware/auth.js";

const router = express.Router();

router.post("/", verifyToken, myPostsController.createPost);
router.delete("/delete/:id", verifyToken, myPostsController.deletePost);
router.patch("/edit/:id", verifyToken, myPostsController.editPost);
router.get("/all", myPostsController.getMyPosts);
// router.get("/:userId/posts", postsController.getUserPosts);
export default router;
