import express from "express";
import * as postsController from "../controllers/posts.js";
import { verifyToken } from "../middlware/auth.js";

const router = express.Router();

router.get("/", verifyToken, postsController.getFeedPosts);
router.get("/:userId/posts", postsController.getUserPosts);
router.patch("/:id/like", verifyToken, postsController.likePost);
export default router;
