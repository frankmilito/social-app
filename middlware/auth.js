import jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {
  try {
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith("Bearer ")
    ) {
      token = req.headers.authorization.split(" ")[1];
    }

    if (!token)
      return res
        .status(401)
        .json({ status: "fail", message: "Unauthorized access" });

    const verified = jwt.verify(token, process.env.JWT_SECRET);

    req.user = verified;
    next();
  } catch (e) {
    res.status(500).json({ status: "error", message: e.message });
  }
};
